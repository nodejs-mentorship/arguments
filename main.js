const yargs = require("yargs");

yargs.command(
  "add",
  "Add new note",
  (yargs) => {
    yargs
      .options("title", {
        describe: "Note title",
        type: "string",
        demandOption: true,
      })
      .option("body", { describe: "Note body", type: "string" });
  },
  (argv) => {
    console.log(`New title:`);
    console.log(argv.title);
    console.log(argv.body);
  }
).argv;

yargs.command(
  "remove",
  "Removes specified note",
  (yargs) => {
    yargs.positional("title", {
      describe: "Title of note to delete",
      type: "string",
      demandOption: true,
    });
  },
  (argv) => {
    console.log(argv);
    console.log(`Title: ${argv._[1]} removed successfully`);
  }
).argv;

yargs.command("list", "Lists all note titles", () => {
  console.log(`title1, title2, title3`);
}).argv;

yargs.command(
  "read",
  "Read specified note",
  (yargs) => {
    yargs.positional("title", {
      describe: "Title of note to read",
      type: "string",
      demandOption: true,
    });
  },
  (argv) => {
    console.log(`${argv._[1]}: Some note`);
  }
).argv;
